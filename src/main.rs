use poise::serenity_prelude as serenity;
use std::fs;

type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>;
struct Data {}

/// Chercher quelqu'un à partir d'un matricule, partie de nom ou prénom
#[poise::command(slash_command, prefix_command)]
async fn lookup(ctx: Context<'_>, #[description = "Recherche"] recherche: String) -> Result<(), Error> {
    // Mettre la recherche en lowercase pour la rendre case insensitive
    let recherche = recherche.to_lowercase();

    // Importer le fichier helmolist.csv
    let fichier = fs::read_to_string("helmolist.csv").expect("Fichier illisible");
    let lignes: Vec<&str> = fichier.split("\n").collect();

    // Créer une réponse
    ctx.send(|b| {
        let mut compteur = 0;
        for ligne in lignes {
            // Mettre la ligne en lowercase pour rendre la recherche case insensitive
            let low_ligne = ligne.to_lowercase();
            if low_ligne.contains(&recherche) {
                compteur += 1;

                // Extraire les informations d'une ligne
                let vec_ligne: Vec<&str> = ligne.split(",").collect();
                let matricule = vec_ligne[0];
                let nom = vec_ligne[1];

                // Discord ne supporte pas plus de 10 embeds dans un message
                if compteur <= 10 {
                    b.embed(|e| {
                        e.colour(serenity::Colour::DARK_GREEN)
                        .title(recherche.to_string())
                        .field("Matricule".to_string(), matricule.to_string(), true)
                        .field("Nom".to_string(), nom.to_string(), true)
                    });
                }
            }
        }

        if compteur == 0 {
            b.embed(|e| {
                e.colour(serenity::Colour::RED)
                .title(recherche.to_string())
                .description("Aucun résultat pour cette recherche")
            });
        }

        let content = format!("{} personne(s) trouvée(s) pour `{}`", compteur, recherche);
        b.content(content).ephemeral(true)
    }).await?;

    Ok(())
}

#[poise::command(prefix_command)]
async fn register(ctx: Context<'_>) -> Result<(), Error> {
    poise::builtins::register_application_commands_buttons(ctx).await?;
    Ok(())
}

#[tokio::main]
async fn main() {
    let framework = poise::Framework::builder()
        .options(poise::FrameworkOptions {
            commands: vec![lookup(), register()],
            ..Default::default()
        })
        .token(std::env::var("DISCORD_TOKEN").expect("DISCORD_TOKEN manquant"))
        .intents(serenity::GatewayIntents::non_privileged())
        .user_data_setup(move |_ctx, _ready, _framework| Box::pin(async move { Ok(Data {}) }));

    framework.run().await.unwrap();
}

